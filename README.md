# DiscordSlackBuilder
[![Discord API](https://discordapp.com/api/guilds/81384788765712384/widget.png)](https://discord.gg/Discord-API)  
A slack message builder for Discord.

#### Used software
* [Highlight.JS](https://highlightjs.org/)
* [Highlight.JS Line Numbers](https://github.com/wcoder/highlightjs-line-numbers.js/)
* [Marked](https://github.com/chjj/marked)

License
----

MIT