dsb = {};

dsb.hook = {};
dsb.element = null;
dsb.hook.username = "";
dsb.hook.icon_url = "";
dsb.hook.text = "";
dsb.hook.attachments = [];

dsb.displayHook = function () {
    if (dsb.element == null) {
        dsb.element = document.createElement("div");
        dsb.element.className = "message-group hide-overflow";
    }
    dsb.element.innerHTML = '<div class="avatar-large"></div><div class="comment"><div class="message"><div class="body"><h2><span class="username-wrapper"><strong class="user-name">'+dsb.hook.username+'</strong><span class="bot-tag">BOT</span></span><span class="highlight-separator"> - </span><span class="timestamp">'+dsb.getCurrentDate()+'</span></h2><div class="message-text"><div class="btn-option"></div><div class="markup">'+dsb.getFormattedMarkdown()+'</div></div></div><div class="accessory"></div></div></div>';

    if (dsb.hook.icon_url == "") {
        dsb.element.children[0].style.backgroundImage = 'url("https://discordapp.com/assets/6debd47ed13483642cf09e832ed0bc1b.png")';
    }
    else {
        dsb.element.children[0].style.backgroundImage = 'url("'+dsb.hook.icon_url+'")';
    }

    $.each(dsb.hook.attachments, function(index, value) {
        var attachment = document.createElement("div");
        attachment.className = "embed embed-rich";
        attachment.style.borderLeftColor = value.color;
        attachment.dataset["id"] = index;
        /*attachment.onclick = function () {
            dsb.showAttachmentSettings(index);
            return false;
        };*/

        if (value.author_name != "") {
            var embedContent = document.createElement("div");
            embedContent.className = "embed-content";
            embedContent.innerHTML = '<div>' +
                (value.author_icon == "" ? '' : '<img class="embed-author-icon" src="'+value.author_icon+'">') +
                '<a class="embed-author" href="'+(value.author_link == "" ? value.title_link : value.author_link)+'" target="_blank" rel="noreferrer">'+value.author_name+'</a>' +
                '</div>';
            attachment.appendChild(embedContent);
        }

        if (value.title != "") {
            var embedTitle = document.createElement("div");
            embedTitle.innerHTML = '<div>' +
                '<a class="embed-title"' +
                (value.title_link == "" ? '' : ' href="'+value.title+'" target="_blank" rel="noreferrer"') +
                '>'+value.title+'</a>' +
                '</div>';
            attachment.appendChild(embedTitle);
        }

        if (value.pretext != "") {
            var pretext = document.createElement("div");
            pretext.className = "embed-description markup";
            pretext.innerHTML = dsb.formatMarkdown(value.pretext);
            attachment.appendChild(pretext);
        }

        if (value.fields.length > 0) {
            var embedFields = document.createElement("div");
            embedFields.className = 'embed-fields';
            $.each(value.fields, function(fIndex, fValue) {
                var field = document.createElement("div");
                field.className = "embed-field";
                field.dataset["parent"] = index;
                field.dataset["id"] = fIndex;

                if (fValue.short) {
                    field.classList.add("embed-field-inline");
                }
                field.innerHTML = '<div class="embed-field-name">'+fValue.title+'</div><div class="embed-field-value markup">'+dsb.formatMarkdown(fValue.value)+'</div>';
                field.innerHTML += '<div style="flex-direction: row;" class="controlButtons"><div class="btn btn-default btn-xs" onclick="dsb.showFieldSettings('+index+', '+fIndex+')"><i class="glyphicon glyphicon-edit"></i></div> <div class="btn btn-primary btn-xs" onclick="dsb.removeField('+index+', '+fIndex+')"><i class="glyphicon glyphicon-remove"></i></div></div>';
                embedFields.appendChild(field);
            });
            attachment.appendChild(embedFields);
        }

        if (value.image_url != "") {
            var embedImage = document.createElement("a");
            embedImage.className = 'embed-thumbnail embed-thumbnail-rich';
            if (value.title_link != "") {
                embedImage.href = value.title_link;
                embedImage.target = "_blank";
                embedImage.rel = "noreferrer";
            }
            embedImage.innerHTML = '<img class="image" src="'+value.image_url+'">';
            attachment.appendChild(embedImage);
        }

        if (value.footer != "") {
            var footer = document.createElement("div");
            footer.innerHTML = '';
            if (value.footer_icon != "") {
                footer.innerHTML += '<img class="embed-footer-icon" src="'+value.footer_icon+'">';
            }
            footer.innerHTML += '<span class="embed-footer">'+value.footer+(value.ts > 0 ? ' | '+dsb.unixToDate(value.ts) :'')+'</span>';
            attachment.appendChild(footer);
        }
        attachment.innerHTML += '<div class="controlButtons"><div class="btn btn-default btn-xs" onclick="dsb.showAttachmentSettings('+index+')"><i class="glyphicon glyphicon-edit"></i></div> <div class="btn btn-primary btn-xs" onclick="dsb.removeAttachment('+index+')"><i class="glyphicon glyphicon-remove"></i></div></div>';
        dsb.element.children[1].children[0].children[1].appendChild(attachment);

        if (value.thumb_url != "") {
            var thumbnail = document.createElement("div");
            thumbnail.className = "embed-rich-thumb";
            thumbnail.innerHTML = '<a class="embed-thumbnail embed-thumbnail-rich"'+(value.title_link == "" ? '' : ' href="'+value.title_link+'" target="_blank" rel="noreferrer"><img class="image" src="'+value.thumb_url+'"></a>');
            dsb.element.children[1].children[0].children[1].appendChild(thumbnail);
        }
    });

    document.getElementById("webhook-container").innerHTML = '';
    document.getElementById("webhook-container").appendChild(dsb.element);
};

dsb.updateHook = function() {
    dsb.hook.username = document.getElementById("webhook-username").value;
    dsb.hook.icon_url = document.getElementById("webhook-icon_url").value;
    dsb.hook.text = document.getElementById("webhook-text").value;

    dsb.displayHook();
};

dsb.addAttachment = function() {
    dsb.hook.attachments[dsb.hook.attachments.length] = {
        "fallback": "",
        "color": "#36a64f",
        "pretext": "New Attachment #"+dsb.hook.attachments.length,
        "author_name": "",
        "author_link": "",
        "author_icon": "",
        "title": "",
        "title_link": "",
        "fields": [],
        "image_url": "",
        "thumb_url": "",
        "footer": "",
        "footer_icon": ""
    };

    dsb.updateHook();
};

dsb.addField = function(id) {
    dsb.hook.attachments[id].fields[dsb.hook.attachments[id].fields.length] = {
        "title": "New Field #"+dsb.hook.attachments[id].fields.length,
        "value": "",
        "short": false
    };

    dsb.updateHook();
};

dsb.removeAttachment = function(id) {
    dsb.hook.attachments.splice(id, 1);

    dsb.updateHook();
};

dsb.removeField = function(parent, id) {
    dsb.hook.attachments[parent].fields.splice(id, 1);

    dsb.updateHook();
};

dsb.getCurrentDate = function() {
    return 'today at 13:37';
};

dsb.formatMarkdown = function(text) {
    if (text == null) {
        text = "";
    }
    return marked(text);
};

dsb.getFormattedMarkdown = function() {
    return dsb.formatMarkdown(dsb.hook.text);
};

dsb.importWebhook = function () {
    dsb.hook = JSON.parse(document.getElementById("webhook-import").value);
    document.getElementById("webhook-username").value = dsb.hook.username;
    document.getElementById("webhook-icon_url").value = dsb.hook.icon_url;
    document.getElementById("webhook-text").value = dsb.hook.text;

    dsb.displayHook();
};

dsb.exportWebhook = function() {
    if (!document.getElementById("webhook-export").classList.contains("show")) {
        document.getElementById("webhook-export").classList.remove("hidden");
    }
    document.getElementById("webhook-export").innerHTML = '<code class="json">'+JSON.stringify(dsb.hook, null, 2)+'</code>';
    hljs.highlightBlock(document.getElementById("webhook-export").children[0]);
    hljs.lineNumbersBlock(document.getElementById("webhook-export").children[0]);
};

dsb.showAttachmentSettings = function(index) {
    if (dsb.hook.attachments.length <= index) {console.error("Item could not be found"); return 0;}

    var settingsContainer = document.getElementById("webhook-settings");
    settingsContainer.innerHTML = '<div class="form-horizontal">' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="attachment-fallback">Fallback</label>' +
        '<div class="col-md-9">' +
        '<textarea class="form-control" id="attachment-fallback" name="attachment-fallback">'+dsb.hook.attachments[index].fallback+'</textarea>' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="attachment-color">Color</label>' +
        '<div class="col-md-9">' +
        '<input class="form-control input-md" id="attachment-color" name="attachment-color" type="text" placeholder="" value="'+dsb.hook.attachments[index].color+'">' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="attachment-pretext">Pretext</label>' +
        '<div class="col-md-9">' +
        '<textarea class="form-control" id="attachment-pretext" name="attachment-pretext">'+dsb.hook.attachments[index].pretext+'</textarea>' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="attachment-author_name">Author name</label>' +
        '<div class="col-md-9">' +
        '<input class="form-control input-md" id="attachment-author_name" name="attachment-author_name" type="text" placeholder="" value="'+dsb.hook.attachments[index].author_name+'">' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="attachment-author_link">Author link</label>' +
        '<div class="col-md-9">' +
        '<input class="form-control input-md" id="attachment-author_link" name="attachment-author_link" type="text" placeholder="" value="'+dsb.hook.attachments[index].author_link+'">' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="attachment-author_icon">Author icon</label>' +
        '<div class="col-md-9">' +
        '<input class="form-control input-md" id="attachment-author_icon" name="attachment-author_icon" type="text" placeholder="" value="'+dsb.hook.attachments[index].author_icon+'">' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="attachment-title">Title</label>' +
        '<div class="col-md-9">' +
        '<input class="form-control input-md" id="attachment-title" name="attachment-title" type="text" placeholder="" value="'+dsb.hook.attachments[index].title+'">' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="attachment-title_link">Title link</label>' +
        '<div class="col-md-9">' +
        '<input class="form-control input-md" id="attachment-title_link" name="attachment-title_link" type="text" placeholder="" value="'+dsb.hook.attachments[index].title_link+'">' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="attachment-image_url">Image URL</label>' +
        '<div class="col-md-9">' +
        '<input class="form-control input-md" id="attachment-image_url" name="attachment-image_url" type="text" placeholder="" value="'+dsb.hook.attachments[index].image_url+'">' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="attachment-thumb_url">Thumb URL</label>' +
        '<div class="col-md-9">' +
        '<input class="form-control input-md" id="attachment-thumb_url" name="attachment-thumb_url" type="text" placeholder="" value="'+dsb.hook.attachments[index].thumb_url+'">' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="attachment-footer">Footer</label>' +
        '<div class="col-md-9">' +
        '<input class="form-control input-md" id="attachment-footer" name="attachment-footer" type="text" placeholder="" value="'+dsb.hook.attachments[index].footer+'">' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="attachment-footer_icon">Footer icon</label>' +
        '<div class="col-md-9">' +
        '<input class="form-control input-md" id="attachment-footer_icon" name="attachment-footer_icon" type="text" placeholder="" value="'+dsb.hook.attachments[index].footer_icon+'">' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="webhook-fields">Fields</label>' +
        '<div class="col-md-9">' +
        '<button id="webhook-fields" name="webhook-fields" class="btn-sm btn-success" onclick="dsb.addField('+index+')">Add</button>' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<div class="col-md-9">' +
        '<button class="btn-xs btn-primary" onclick="dsb.saveAttachment('+index+')">Save changes</button>' +
        '</div>' +
        '</div>' +

        '</div>';
};

dsb.showFieldSettings = function(parent, index) {
    if (dsb.hook.attachments[parent].fields.length <= index) {console.error("Item could not be found"); return 0;}

    var settingsContainer = document.getElementById("webhook-settings");
    settingsContainer.innerHTML = '<div class="form-horizontal">' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="field-title">Title</label>' +
        '<div class="col-md-9">' +
        '<input class="form-control input-md" id="field-title" name="field-title" type="text" placeholder="" value="'+dsb.hook.attachments[parent].fields[index].title+'">' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="field-value">Value</label>' +
        '<div class="col-md-9">' +
        '<input class="form-control input-md" id="field-value" name="field-value" type="text" placeholder="" value="'+dsb.hook.attachments[parent].fields[index].value+'">' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<label class="col-md-3 control-label" for="field-short">Short</label>' +
        '<div class="col-md-9">' +
        '<select class="form-control" id="field-short" name="field-short">' +
        '<option value="true"'+(dsb.stringToBool(dsb.hook.attachments[parent].fields[index].short) ? ' selected' : '')+'>True</option>' +
        '<option value="false"'+(dsb.stringToBool(dsb.hook.attachments[parent].fields[index].short) ? '' : ' selected')+'>False</option>' +
        '</select>' +
        '</div>' +
        '</div>' +

        '<div class="form-group">' +
        '<div class="col-md-9">' +
        '<button class="btn-sm btn-primary" onclick="dsb.saveField('+parent+','+index+')">Save changes</button>' +
        '</div>' +
        '</div>' +

        '</div>';
};

dsb.saveAttachment = function (index) {
    dsb.hook.attachments[index].fallback = document.getElementById("attachment-fallback").value;
    dsb.hook.attachments[index].color = document.getElementById("attachment-color").value;
    dsb.hook.attachments[index].pretext = document.getElementById("attachment-pretext").value;
    dsb.hook.attachments[index].author_name = document.getElementById("attachment-author_name").value;
    dsb.hook.attachments[index].author_link = document.getElementById("attachment-author_link").value;
    dsb.hook.attachments[index].author_icon = document.getElementById("attachment-author_icon").value;
    dsb.hook.attachments[index].title = document.getElementById("attachment-title").value;
    dsb.hook.attachments[index].title_link = document.getElementById("attachment-title_link").value;
    dsb.hook.attachments[index].image_url = document.getElementById("attachment-image_url").value;
    dsb.hook.attachments[index].thumb_url = document.getElementById("attachment-thumb_url").value;
    dsb.hook.attachments[index].footer = document.getElementById("attachment-footer").value;
    dsb.hook.attachments[index].footer_icon = document.getElementById("attachment-footer_icon").value;

    dsb.updateHook();
};

dsb.saveField = function (parent, index) {
    dsb.hook.attachments[parent].fields[index].title = document.getElementById("field-title").value;
    dsb.hook.attachments[parent].fields[index].value = document.getElementById("field-value").value;
    dsb.hook.attachments[parent].fields[index].short = dsb.stringToBool(document.getElementById("field-short").value);

    dsb.updateHook();
};

dsb.getVariableType = function (variable) {
    return typeof variable;
};

dsb.stringToBool = function (input) {
    return Boolean(input);
};

dsb.sendWebhook = function () {
    var id = document.getElementById("webhook-hook_id").value;
    var key = document.getElementById("webhook-hook_key").value;

    var http = new XMLHttpRequest();
    var url = 'https://discordapp.com/api/webhooks/'+id+'/'+key+'/slack';
    var params = JSON.stringify(dsb.hook);
    http.open("POST", url, true);

    http.setRequestHeader("Content-Type", "application/json");
    http.setRequestHeader("User-Agent", "DiscordSlackBuilder (https://nick-strohm.github.io, 1.2.1)");

    http.onload = function () {
        var header = document.getElementById("webhook-hook_header");
        header.innerText = http.getAllResponseHeaders();
        console.log(http.getAllResponseHeaders());
        /*$.each(http.getAllResponseHeaders(), function(index, value) {
            header.innerText += value.key+': '+value.value;
        });*/

        //var response = document.getElementById("webhook-hook_response");
        header.innerText += http.responseText;
    };

    http.send(params);

    console.log(url);
    console.log(params);
};

dsb.changeTheme = function () {
    var pressed = document.getElementById("webhook-page_theme").checked;

    if (pressed) {
        document.getElementsByTagName("head")[0].children[2].href = 'http://bootswatch.com/cyborg/bootstrap.min.css';
        document.getElementsByTagName("body")[0].classList.add("dark");
    }
    else {
        document.getElementsByTagName("head")[0].children[2].href = 'http://bootswatch.com/paper/bootstrap.min.css';
        document.getElementsByTagName("body")[0].classList.remove("dark");
    }
};

dsb.beginsWith = function (needle, haystack) {
    return (haystack.substr(0, needle.length) == needle);
};